#!/usr/bin/env node
var WebSocketServer = require('websocket').server;
var https = require('https');
const fs = require('fs');
const IP_ADDRESS_OR_HOST = require('ip').address();
console.log(IP_ADDRESS_OR_HOST);
/**
 * openssl req -x509 -out localhost.crt -keyout localhost.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj '/CN=localhost' -extensions EXT -config <( \
   printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
 */

const acceptedOrigins = ['abi-protocol','echo-protocol'];
const acceptedProtocols = ['abi-protocol','echo-protocol'];

const options = {
    key: fs.readFileSync('localhost.key'),
    cert: fs.readFileSync('localhost.crt')
};

var server = https.createServer(options, (request, response)=> {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});
server.listen(8080, IP_ADDRESS_OR_HOST, function() {
    console.log((new Date()) + `Server ==> ${IP_ADDRESS_OR_HOST} is listening on port 8080`);
});

wsServer = new WebSocketServer({
    httpServer: server,
    // You should not use autoAcceptConnections for production
    // applications, as it defeats all standard cross-origin protection
    // facilities built into the protocol and the browser.  You should
    // *always* verify the connection's origin and decide whether or not
    // to accept it.
    autoAcceptConnections: false
});

function protocolIsAllowed(protocol) {
    // put logic here to detect whether the specified protocol is allowed.
    const found = acceptedOrigins.some(someProtocol => protocol.indexOf(someProtocol) >= 0);
    console.log('PROTOCOL - ', protocol, '=> ', found);
    return found;
  }

function originIsAllowed(origin) {
  // put logic here to detect whether the specified origin is allowed.
  const found = acceptedOrigins.some(someOrigin => origin.indexOf(someOrigin) >= 0);
  console.log('ORIGIN - ', origin, '=> ', found);
  return found;
}

wsServer.on('request', function(request) {
    console.log('headers', request.headers);
    if (!originIsAllowed(request.origin)) {
      // Make sure we only accept requests from an allowed origin
      request.reject();
      console.log((new Date()) + ' SECURE Connection from origin ' + request.origin + ' rejected.');
      return;
    }
    if(!protocolIsAllowed(request.requestedProtocols)){
        // Make sure we only accept requests from an allowed protocols
      request.reject();
      console.log((new Date()) + ' SECURE Connection from protocol ' + request.requestedProtocols + ' rejected.');
      return;
    }
    
    ///                 request.requestedProtocols , request.origin
    var connection = request.accept('abi-protocol', request.origin);
    console.log((new Date()) + ' SEUCURE Connection accepted.');
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log('Received Message: ' + message.utf8Data);
            connection.sendUTF(message.utf8Data);
        }
        else if (message.type === 'binary') {
            console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
            connection.sendBytes(message.binaryData);
        }
    });

    connection.on('close', function(reasonCode, description) {
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});