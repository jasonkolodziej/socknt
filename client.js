#!/usr/bin/env node
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
var WebSocketClient = require('websocket').client;
const https = require('https');
const fs = require('fs');

var client = new WebSocketClient({tlsOptions: {    ca: [ fs.readFileSync('localhost.crt') ] }});

client.on('connectFailed', function(error) {
    console.log('Connect Failure: ' + error.toString());
});

client.on('connect', function(connection) {
    console.log('SECURE - WebSocket Client Connected');
    connection.on('error', function(error) {
        console.log("Connection Error: " + error.toString());
    });
    connection.on('close', function() {
        console.log('echo-protocol Connection Closed');
    });
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log("Received: '" + message.utf8Data + "'");
        }
    });
    connection.on('message', function(message) {
        if (message.type === 'binary') {
            console.log("Received: '" + message.binaryData + "'");
        }
    });
    
    function sendNumber() {
        if (connection.connected) {
            var number = Math.round(Math.random() * 0xFFFFFF);
            connection.sendBytes(new Buffer.from(number.toString()));
            setTimeout(sendNumber, 1000);
        }
    }
    sendNumber();
});
let header = {token: 'ugh'};
  //                                                                              Headers obj, requestOptions
client.connect('wss://192.168.1.107:8080/', 'abi-protocol',['abi-protocol','echo-protocol'],header, null);